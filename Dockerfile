FROM node:20.11.0-alpine

# Install necessary packages
RUN apk add --no-cache openjdk11 maven \
    && apk update \
    && apk upgrade 

RUN npm install -g @vue/cli

