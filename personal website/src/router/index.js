import { createRouter, createWebHistory } from "vue-router";
import HomeView from "../views/HomeView.vue";

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: "/",
      name: "",
      component: HomeView,
    },
    {
      path: "/home",
      name: "home",
      component: HomeView,
    },
    {
      path: "/bildungsweg",
      name: "bildungsweg",
      component: () => import("../components/Bildungsweg.vue"),
    },
    {
      path: "/erfahrungen",
      name: "erfahrungen",
      component: () => import("../components/Erfahrungen.vue"),
    },
  ],
});

export default router;
